module.exports = {
    locales: {
      '/': {
        lang: 'ja',
        title: 'Cokesのvuepress'
      },
      '/en/': {
        lang: 'en-US',
        title: 'Cokes VuePress'
      }
    },
    title: 'Cokes ❤️ GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress-cokes/',
    dest: 'public'
}
